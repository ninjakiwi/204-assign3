import java.net.Socket;
import java.net.InetAddress;
import java.net.SocketTimeoutException;
import java.net.SocketAddress;
import java.net.InetSocketAddress​;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.File;
import java.io.FileOutputStream;
import java.nio.ByteBuffer;
import java.net.DatagramSocket;
import java.net.DatagramPacket;

import java.util.concurrent.TimeUnit;

public class TftpClient {

	public static void main(String[] args) {
		if (args.length != 4) {
			System.out.println("Usage: TftpClient <address> <port> <file> <output_file>");
			return;
		}

		// Attempt to open a socket to server
		InetSocketAddress​ serverSocAddr;
		try {
			// Get internet address
			InetAddress​ serverAddr = InetAddress.getByName(args[0]);
			serverSocAddr = new InetSocketAddress​(serverAddr, Integer.parseInt(args[1]));

			System.out.println("Requesting file from " + serverAddr.getHostName() + ":" + args[1]);
		} catch (Exception ex) {
			System.out.println("Failed to open socket: " + ex);
			return;
		}

		// Get the file
		TftpFileRequest request = new TftpFileRequest(serverSocAddr, args[2], args[3]);
	}
	
}

class TftpFileRequest {
	private DatagramSocket clientSocket;
	private SocketAddress​ serverSocAddr;
	private static final int TIME_OUT = 1000; //1 second
	private static final int MAX_RESEND = 5;
	private static final int DATA_BUFFER_SIZE = 512;
	private static final byte RRQ = 1;
	private static final byte DATA = 2;
	private static final byte ACK = 3;
	private static final byte ERROR = 4;

	public TftpFileRequest(InetSocketAddress​ socAddr, String fetchFile, String outputFile) {
		serverSocAddr = socAddr;

		// Ask for file and save response
		try {
			clientSocket = new DatagramSocket();
			clientSocket.send(packFileRequest(fetchFile));
			System.out.println("Sent request");
			recieveFile(outputFile);
		} catch (Exception ex) {
			System.out.println("Failed to get file: " + ex);
		}

		clientSocket.close();
	}

	private void recieveFile(String saveTo) throws Exception {
		byte currentBlockSeq = 1;
		FileOutputStream fileOutput = new FileOutputStream(saveTo);
		TftpFileBuffer dataBuffer = new TftpFileBuffer();
		while (true) {

			//wait for data packet and send ACK
			if (!recieveDataPacket(dataBuffer, currentBlockSeq)) {
				System.out.println("Failed to get file");
				break;
			}

			fileOutput.write(dataBuffer.data, 0, dataBuffer.length);

			// the last packet (the file size if not a multiple of 512)
			if (dataBuffer.length < DATA_BUFFER_SIZE) {
				System.out.println("Received last packet ["+dataBuffer.length+" bytes in size]:#"+(currentBlockSeq & 0xFF));
				break;
			} else {
				System.out.println("Received data block #"+(currentBlockSeq & 0xFF));
			}
			currentBlockSeq++;
		}

		fileOutput.close();
	}

	private boolean recieveDataPacket(TftpFileBuffer data, byte blockSeqNumber) throws Exception {
		int resendCount = 0;
		DatagramPacket packet = new DatagramPacket(new byte[1472], 1472);

		//try (MAX_RESEND) times
		while (resendCount < MAX_RESEND) {
			try {
				// Receive DATA packet
				clientSocket.setSoTimeout(TIME_OUT);
				clientSocket.receive(packet);
				byte[] packetData = packet.getData();

				serverSocAddr = packet.getSocketAddress();

				if (packetData[0] == ERROR) {
					printErrorString(packet);
					return false;
				}
				if (packetData[0] != DATA) {
					// Not a data packet, cancel transaction
					return false;
				}

				// If the packet's sequence number is different from what is
				// expected, resend ACK if smaller than expected, bail if larger
				if (packetData[1] > blockSeqNumber) {
					System.out.println("Unexpected block sequence number");
					var thing = packErrorPacket("Missing block #" + blockSeqNumber + ", terminating transaction");
					clientSocket.send(thing);
					return false;
				} else if (packetData[1] < blockSeqNumber) {
					System.out.println("Already had this block, resending acknowledge packet");
					// Resend acknowledge packet
					clientSocket.send(packACK(packetData[1]));
					resendCount++;
					continue;
				}

				clientSocket.send(packACK(blockSeqNumber));

				// Got and acknowledged data packet
				data.length = packet.getLength() - 2;
				ByteBuffer byteBuffer = ByteBuffer.allocate(data.length);
				byteBuffer.put(packetData, 2, data.length);
				data.data = byteBuffer.array();
				return true;

			} catch(SocketTimeoutException ex) {
				resendCount++;
				System.out.println("Timeout #" + resendCount);
			}
		}

		return false;
	}

	private DatagramPacket packACK(byte blockSeqNumber) {
		int dataLength = 2;
		ByteBuffer byteBuffer = ByteBuffer.allocate(dataLength);
		byteBuffer.put(ACK);
		byteBuffer.put(blockSeqNumber);
		return new DatagramPacket(byteBuffer.array(), dataLength, serverSocAddr);
	}

	private DatagramPacket packFileRequest(String filename) throws Exception {
		byte[] payload = filename.getBytes();
		int dataLength = 1 + payload.length;
		ByteBuffer byteBuffer = ByteBuffer.allocate(dataLength);
		byteBuffer.put(RRQ);
		byteBuffer.put(payload);
		return new DatagramPacket(byteBuffer.array(), dataLength, serverSocAddr);
	}

	private DatagramPacket packErrorPacket(String errorMessage) throws Exception {
		byte[] payload = errorMessage.getBytes();
		int dataLength = 1 + payload.length;
		ByteBuffer byteBuffer = ByteBuffer.allocate(dataLength);
		byteBuffer.put(ERROR);
		byteBuffer.put(payload);
		return new DatagramPacket(byteBuffer.array(), dataLength, serverSocAddr);
	}

	private static void printErrorString(DatagramPacket packet){
		byte[] data = packet.getData();
		int dataLength = packet.getLength();
		ByteBuffer byteBuffer = ByteBuffer.allocate(dataLength-1);
		// ignore the packet type
		byteBuffer.put(data, 1, dataLength-1);
		System.out.println("Server: " + new String(byteBuffer.array()));
	}
}

class TftpFileBuffer {
	public int length;
	public byte[] data;
}
