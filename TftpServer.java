import java.net.DatagramSocket;
import java.net.DatagramPacket;
import java.net.SocketAddress;
import java.net.SocketTimeoutException;
import java.io.File;
import java.io.FileInputStream;
import java.nio.ByteBuffer;
import java.io.IOException;
import java.util.concurrent.Executors;
import java.util.concurrent.ExecutorService;

class TftpServer {
	private static final ExecutorService threadPool = Executors.newFixedThreadPool(4);

	public static void main(String[] args) {
		System.out.println("TFTP server starting");

		try {
			DatagramSocket serverSocket = new DatagramSocket(8001);

			//ServerSocket serverSocket = new ServerSocket(8181);
			System.out.println("TFTP server started on port " + serverSocket.getLocalPort());

			while (true) {
				byte[] buffer = new byte[1472];
				DatagramPacket packet = new DatagramPacket(buffer, 1472);
				serverSocket.receive(packet);
				TftpServerWorker newTask = new TftpServerWorker(packet);
				threadPool.execute(newTask);
			}
		} catch (Exception ex) {
			System.out.println("Server crashed, exception: " + ex);
		}
	}
}

class TftpServerWorker implements Runnable {
	//use to store the Read Request (RRQ)
	private DatagramPacket request;
    private DatagramSocket socket;
    private SocketAddress clientAddress;

	private static final int TIME_OUT = 1000; //1 second
	private static final int MAX_RESEND = 5;
	private static final int DATA_BUFFER_SIZE = 512;
	private static final byte RRQ = 1;
	private static final byte DATA = 2;
	private static final byte ACK = 3;
	private static final byte ERROR = 4;

	@Override
	public void run() {
		try {
			socket = new DatagramSocket();
		} catch (Exception ex) {
			return;
		}

		try {
			System.out.println("TftpServer sending file on port " + socket.getLocalPort());

			clientAddress = request.getSocketAddress();

			byte[] requestData = request.getData();

			// Get packet type of the initial packet
			byte requestType = requestData[0];

			if (requestType != RRQ) {
				socket.close();
				return;
			}

			// Get filename (remove RRQ byte)
			int dataLength = request.getLength();
			ByteBuffer byteBuffer = ByteBuffer.allocate(dataLength-1);
			byteBuffer.put(requestData, 1, dataLength-1);
			String filename = new String(byteBuffer.array());
			File file = new File("./" + filename);

			if (file.exists()) {
				sendFile(file);
			} else {
				System.out.println(filename + " doesn't exist on the server");
				DatagramPacket errorDP = packErrorPacket(filename + " not found");
				errorDP.setSocketAddress(clientAddress);
				socket.send(errorDP);
			}

		} catch (Exception ex) {
			// Failed to get request
			System.out.println("Exception: " + ex);
		}

		// Communication finished, attempt to close the socket
		socket.close();
	}

	private void sendFile(File file) throws Exception {
		byte currentBlockSeq = 1;
		FileInputStream fileInput = new FileInputStream(file);
		byte[] dataBuffer = new byte[DATA_BUFFER_SIZE];
		while (true) {
			int dataByte = fileInput.read(dataBuffer);

			//the file size is a multiple of 512, send empty packet
			if (dataByte == -1) dataByte = 0;
			
			System.out.println("Sending data block #"+(currentBlockSeq & 0xFF));

			//send data packet and wait for ACK
			if (!sendDataPacket(dataBuffer, dataByte, currentBlockSeq)) {
				System.out.println("Failed to send file");
				break;
			}

			// the last packet (the file size if not a multiple of 512)
			if (dataByte < DATA_BUFFER_SIZE) {
				System.out.println("Last packet sent (#"+(currentBlockSeq & 0xFF)+") was "+dataByte+" bytes in size");
				break;
			}

			currentBlockSeq++;
		}

		fileInput.close();
	}

	private boolean sendDataPacket(byte[] databuffer, int length, byte blockSeqNumber) throws Exception {
		int resendCount = 0;
		DatagramPacket dataPacket = packFileDataPacket(databuffer, length, blockSeqNumber);

		DatagramPacket ackDP = new DatagramPacket(new byte[1472], 1472);

		//try (MAX_RESEND) times
		while (resendCount < MAX_RESEND) {
			try {
				socket.send(dataPacket);

				// Receive ACK packet
				socket.setSoTimeout(TIME_OUT);
				socket.receive(ackDP);
				byte[] ackDPData = ackDP.getData();
				if (ackDPData[0] == ERROR) {
					printErrorString(ackDP);
					return false;
				} else if (ackDPData[0] != ACK || ackDPData[1] != blockSeqNumber) {
					// The packet received is not a ACK packet or the sequence number
					// is different from the sent packet, ignore ack and resend
					resendCount++;
					continue;
				}

				System.out.println("Received ack #" + (ackDPData[1] & 0xFF));

				// Sent packet has been acknowledged
				return true;

			} catch(SocketTimeoutException ex) {
				resendCount++;
				System.out.println("Timeout #" + resendCount);
			}
		}

		// Resent max times with no success
		return false;
	}

	private static DatagramPacket packErrorPacket(String errorMessage) throws Exception {
		byte[] payload = errorMessage.getBytes();
		int dataLength = 1 + payload.length;
		ByteBuffer byteBuffer = ByteBuffer.allocate(dataLength);
		byteBuffer.put(ERROR);
		byteBuffer.put(payload);
		return new DatagramPacket(byteBuffer.array(), dataLength);
	}

	private DatagramPacket packFileDataPacket(byte[] dataBuffer, int length, byte blockSeqNumber) {
		int packetLength = 2 + length; // type (1) + block seq (1) + data length
		ByteBuffer byteBuffer = ByteBuffer.allocate(packetLength);
		byteBuffer.put(DATA); // type
		byteBuffer.put(blockSeqNumber); // block seq
		byteBuffer.put(dataBuffer, 0, length); // data
		DatagramPacket dataPacket = new DatagramPacket(byteBuffer.array(), packetLength);
		dataPacket.setSocketAddress(clientAddress);
		return dataPacket;
	}

	private static void printErrorString(DatagramPacket packet){
		byte[] data = packet.getData();
		int dataLength = packet.getLength();
		ByteBuffer byteBuffer = ByteBuffer.allocate(dataLength-1);
		// ignore the packet type
		byteBuffer.put(data, 1, dataLength-1);
		System.out.println(new String(byteBuffer.array()));
	}

	public TftpServerWorker(DatagramPacket packet) {
		request = packet;
	}
}
